const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');
const express = require('express');
const users = require('./routes/users');
const jobs= require('./routes/jobs');
const auth = require('./routes/auth');
const companys=require('./routes/companys');
const applyjobs=require('./routes/applyjobs');

const app = express();

if (!config.get("jwtPrivateKey")) {
    console.error("FATAL Error: JWT Private is not defined." );
    process.exit(1);
}

mongoose.connect(config.connectionstring)
.then(()=> {console.log("Connected to recruitment")})
.catch(()=>{console.error("Error connecting Recruitment")});

app.use(express.json());
app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Access-Control-Allow-Methods',"PUT, GET, POST, DELETE, OPTIONS");
        res.header("Access-Control-Allow-Origin", "*"); // keep this if your api accepts cross-origin requests
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token, auth-token");
        next();
    }
)
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/jobs',jobs);
app.use('/api/companys',companys);
app.use('/api/applyjobs',applyjobs);


const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log(`Listening on port ${port}`));
