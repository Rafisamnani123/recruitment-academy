const mongoose = require("mongoose");

const applyjobSchema = mongoose.Schema({
 
  content: { type: String, required:true },
  Company:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Company',
      required:"company is required feild"

  },
  applicant:{
    type:mongoose.Schema.Types.String,
    ref:'User',
    required:true


},
applicantid:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User',
    required:true
    

},

email:{
    type:mongoose.Schema.Types.String,
    ref:'User',
    required:true

},
status:{
    type:String,
    default:'pending'
}
  
 

});

const Applyjob = mongoose.model("Applyjob", applyjobSchema);

function validateApplyjob(applyjob) {
    const schema = {
        
        content: Joi.string().min(5).max(255).required()
       
       
    };
    return Joi.validate(applyjob, schema);
}


exports.Applyjob =Applyjob ;
exports.validate = validateApplyjob;