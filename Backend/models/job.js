const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const config = require('config');

const jobSchema = new mongoose.Schema({
    yourName: {
        type:String,
        required: true,
        minlength:5,
        maxlength:50
    },
    email:{
        type:String,
        required:true,
        unique: true
    },
    city:{
        type:String,
        required: true

    },
    Field:{
        type:String,
        required:true
    },

    experience:{
        type:String,
        required:true
    },
    expectedSalary:{
        type:String
    },
    candidateid:{
        type: String
    },
    status:{
        type:String,
        default:'pending'
    }
   

   
   
    
    
})
const Job = mongoose.model('job', jobSchema);

 




function validateJob(job) {
    const schema = {
        yourName: Joi.string().min(5).max(50).required(),
        email: Joi.string().required(),
        city: Joi.string().required(),
        Field: Joi.string().required(),
        experience: Joi.string().required(),
        expectedSalary: Joi.string(),
        status: Joi.string(),
    };
    return Joi.validate(job, schema);
}

module.exports.Job = Job;
module.exports.validate = validateJob;


