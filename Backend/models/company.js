const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const config = require('config');

const companySchema = new mongoose.Schema({
    jobName: {
        type:String,
        required: true
        
    },
    experience: {
        type: String,
        required: true
    },
    requiredage: {
        type: String,
        required: true
    },
    salary: {
        type: String,
        required: true,
        
        
     },
    
    jobtype:{
        type:String,
        require:true,
    },
    status:{
        type:String,
        default:"availabe",
    },
    Applicants: [{ type: mongoose.Schema.Types.ObjectId, ref: "Applyjob",}],
  assign: { type: mongoose.Schema.Types.String, ref: "User" }
})



const Company = mongoose.model('Company', companySchema);


function validateCompany(company) {
    const schema = {
        jobName: Joi.string().required(),
        experience: Joi.string().required(),
        requiredage: Joi.string().required(),
        salary: Joi.string().required(),
        jobtype: Joi.string(),
        status: Joi.string()
    };
    return Joi.validate(company, schema);
}

exports.Company = Company;
exports.validate = validateCompany;