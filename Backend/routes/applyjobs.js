const config = require('config');
const jwt = require("jsonwebtoken");
const express = require('express');

const {Applyjob} = require('../models/applyjob');
const router = express.Router();
const bcrypt = require('bcrypt');
const _ = require('lodash');

router.get('/:id',async(req, res)=> {
    Applyjob.find({
        Company: req.params.id
      }).exec(function(err, Applyjob){
        if (err) return res.send(err);
        res.send(Applyjob);
    });
});


router.put('/jobupdate/:id',async(req, res)=> {
  let job = await Applyjob.findByIdAndUpdate(req.params.id,{status:req.body.status})

  res.send({message:"Job Updated"})
});

module.exports = router;