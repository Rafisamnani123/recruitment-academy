const config = require('config');
const jwt = require("jsonwebtoken");
const express = require('express');
const {Job, validate} = require('../models/job');
const router = express.Router();
const _ = require('lodash');
const auth =  require('../middleware/auth');
const candidate =  require('../middleware/candidate');
const admin =  require('../middleware/admin');
const { fileLoader } = require('ejs');

router.post('/', async(req, res)=> {
    
    // let job = await Job.findOne({email: req.body.email});
    // if (user) return res.status(400).send('User already Apply for this job');

    job = new Job({ 
        candidateid: req.body.candidateid,
        yourName: req.body.yourName,
        email:req.body.email,
        city: req.body.city,
        Field: req.body.Field,
        experience: req.body.experience,
        expectedSalary: req.body.expectedSalary,
        status: req.body.status,
        
       
        
    })
    await job.save();
    res.send("job applied");
})

router.get('/all',async(req,res)=>{

    const job =await Job.find();
    
    res.send(job);
})





router.get('/:id',async(req,res)=>{
    const job = await Job.find({candidateid: req.params.id})
    res.send(job);
})

router.put('/updatestatus',async(req,res)=>{
    const job = await Job.find({candidateid: req.params.id})
    res.send(job); 
})

router.put('/status/:id',async(req,res)=>{
    const job = await Job.findByIdAndUpdate(req.params.id,{status: req.body.status})
    res.send(job); 
})




    


router.put('/updatejob/:_id',async(req,res)=>{

    const job = await Job.findById(req.params._id);
    if(!job) return res.status(404).send("user not found");
    job.status = "interview";
    let promises =[];
    promises.push(job.save());
    let result =[]
    result = await Promise.all(promises);
    res.send(job);

})
router.put('/rejectjob/:_id',async(req,res)=>{

    const job = await Job.findById(req.params._id);
    if(!job) return res.status(404).send("user not found");
    job.status = "reject";
    let promises =[];
    promises.push(job.save());
    let result =[]
    result = await Promise.all(promises);
    res.send(job);

})




module.exports = router; 