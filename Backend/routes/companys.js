const config = require('config');
const jwt = require("jsonwebtoken");
const express = require('express');
const {Company, validate} = require('../models/company');
const router = express.Router();
const bcrypt = require('bcrypt');
const _ = require('lodash');
const auth =  require('../middleware/auth');
const admin =  require('../middleware/admin');
const {Job}=require('../models/job');
 const candidate =require('../middleware/candidate');
 const {Applyjob}= require('../models/applyjob');

router.post('/', async(req, res)=> {
    console.log(req.body);
    //req.body = JSON.parse(req.body.body);
    const {error} = validate(req.body);
    if (error) { console.log(error); return res.status(400).send(error.details[0].message);}  
    
    company = new Company({ 
        jobName: req.body.jobName,
        experience: req.body.experience,
        requiredage:req.body.requiredage,
        salary:req.body.salary,
        jobtype:req.body.jobtype,
    });
    
    await company.save();
    //const token = user.generateAuthToken();
    res.send(company);
    //res.header('x-auth-token', token).send(_.pick(user, ["_id","name","email","userType"]));
})

    router.get('/view',  async (req, res) => {
        const company = await Company.find();
        res.send(company);
    });
    
router.put('/updatejob/:_id',async(req,res)=>{

    const company = await Company.findById(req.params._id);
    if(!company) return res.status(404).send("user not found");
    company.status = "Applied";
    company.assign= req.body.assignid
    let promises =[];
    promises.push(company.save());
    let result =[]
    result = await Promise.all(promises);
    res.send(company);

})

router.post("/:id/apply",[auth],async (req,res) =>{
    const companys=await Company.findOne({_id: req.params.id});
    const applyjob=new Applyjob();
   applyjob.content=req.body.content;
    applyjob.Company=req.params.id;
    applyjob.applicant=req.userdata.name;
    applyjob.applicantid=req.userdata._id;
    applyjob.email=req.userdata.email
    await applyjob.save();

    companys.Applicants.push(applyjob._id)
    await companys.save();
    res.send(applyjob);
   
});


module.exports = router;
