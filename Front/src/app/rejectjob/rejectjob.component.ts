//import { Component, OnInit } from '@angular/core';
import { JobService } from './../services/job.service';
//import { Component, OnInit } from '@angular/core';
//import { OrderService } from './../services/order.service';
import { Component, OnInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';

import { NotFoundError } from './../common/not-found-error';
//import { JobService } from './../services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'rejectjob',
  templateUrl: './rejectjob.component.html',
  styleUrls: ['./rejectjob.component.css']
})
export class RejectjobComponent implements OnInit {

  isBooked = false
  isEnd = true

  jobid

  job:any = []
  constructor(private service:JobService,private route:ActivatedRoute, private toastr: ToastrService) { }
id
ride:any=[]
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      if(params.has("id")) {
          
          this.id = params.get('id');    
          this.service.getById(this.id).subscribe(
            data=> {
              this.ride = data
            },
            
          )  
      }
    })

   
  }

  RejectJob(){
    this.isBooked = true
    this.isEnd = false
    let job = {
    ride:this.id,
    isBooked: this.isBooked,
    isComplete: this.isEnd
    
    
    
  
    
    }
    
    
  
    this.service.update(job,"rejectjob",this.id).subscribe(Response => {
      this.toastr.success(`job rejected!`);
      this.jobid=Response._id;
      console.log(this.jobid);
    
  }
   
   
  )
  }


  
  }


