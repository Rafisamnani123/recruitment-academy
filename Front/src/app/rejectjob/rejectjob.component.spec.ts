import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectjobComponent } from './rejectjob.component';

describe('RejectjobComponent', () => {
  let component: RejectjobComponent;
  let fixture: ComponentFixture<RejectjobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectjobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectjobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
