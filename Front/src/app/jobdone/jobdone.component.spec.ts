import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobdoneComponent } from './jobdone.component';

describe('JobdoneComponent', () => {
  let component: JobdoneComponent;
  let fixture: ComponentFixture<JobdoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobdoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobdoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
