import { JobService } from './../services/job.service';
//import { Component, OnInit } from '@angular/core';
//import { OrderService } from './../services/order.service';
import { Component, OnInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';

import { NotFoundError } from './../common/not-found-error';
//import { JobService } from './../services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'jobdone',
  templateUrl: './jobdone.component.html',
  styleUrls: ['./jobdone.component.css']
})
export class JobdoneComponent implements OnInit {

  isBooked = false
  isEnd = true

  jobid

  job:any = []
  constructor(private service:JobService,private route:ActivatedRoute, private toastr: ToastrService) { }
id
ride:any=[]
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      if(params.has("id")) {
          
          this.id = params.get('id');    
          this.service.getById(this.id).subscribe(
            data=> {
              this.ride = data
            },
            
          )  
      }
    })

   
  }

  ConfirmJob(){
    this.isBooked = true
    this.isEnd = false
    let job = {
    ride:this.id,
    isBooked: this.isBooked,
    isComplete: this.isEnd
    
    
    
  
    
    }
    
    
  
    this.service.update(job,"updatejob",this.id).subscribe(Response => {
      this.toastr.success(`job successfully confirmed!`);
      this.jobid=Response._id;
      console.log(this.jobid);
    
  }
   
   
  )
  }


  
  }

