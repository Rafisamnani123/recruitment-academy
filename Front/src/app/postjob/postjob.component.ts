import { JobService } from './../services/job.service';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
//import { JobService } from './../services/order.service';
import { Router } from '@angular/router';
//import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { AppError } from './../common/app-error';
import { BadInput } from './../common/bad-input';



import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'postjob',
  templateUrl: './postjob.component.html',
  styleUrls: ['./postjob.component.css']
})
export class PostjobComponent implements OnInit {

  user:any=[]

  constructor( private toastr: ToastrService,
    private route: ActivatedRoute,
    private service: JobService,
    private auth : AuthService) {


   }

  ngOnInit(): void {
  }

  PostJob(cr){
    alert(1);
    let user = {
      candidateid:this.auth.getCurrentUser()._id,
      //customerid:this.auth.getCurrentUser()._id,
      yourName:cr.value.registration.yourName,
      email:cr.value.registration.email,
      city:cr.value.registration.city,
      Field:cr.value.registration.Field,
      experience:cr.value.registration.experience,
      expectedSalary:cr.value.registration.expectedSalary
      
  
  
  }
  this.service.create(user).subscribe(Response => {
      this.toastr.success(`discription successfully added`)
    },
    (error: AppError) => {
      if (error instanceof BadInput)
        this.toastr.error('Incorrect Inputs');
      else
        throw error
   }
  )
  
  cr.reset();
  
  }
  
  
  }
