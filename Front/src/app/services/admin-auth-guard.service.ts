import { AuthService } from './auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate{

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate() {
    let user = this.authService.getCurrentUser();
    if (user && user.userType.toLowerCase() === 'admin') return true;

    this.router.navigate(['/noaccess']);
    return false;

  }
}
