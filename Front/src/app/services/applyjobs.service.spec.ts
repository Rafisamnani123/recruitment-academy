import { TestBed } from '@angular/core/testing';

import { ApplyjobsService } from './applyjobs.service';

describe('ApplyjobsService', () => {
  let service: ApplyjobsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApplyjobsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
