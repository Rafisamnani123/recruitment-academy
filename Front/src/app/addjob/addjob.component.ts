import { CompanyService } from './../services/company.service';
import { Component, OnInit } from '@angular/core';
//import { CompanyService } from './../services/job.service';
import { AuthService } from './../services/auth.service';
//import { Component, OnInit } from '@angular/core';
//import { JobService } from './../services/order.service';
import { Router } from '@angular/router';
//import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { AppError } from './../common/app-error';
import { BadInput } from './../common/bad-input';



import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'addjob',
  templateUrl: './addjob.component.html',
  styleUrls: ['./addjob.component.css']
})
export class AddjobComponent implements OnInit {

  user:any=[]

  constructor( private toastr: ToastrService,
    private route: ActivatedRoute,
    private service: CompanyService,
    private auth : AuthService) {


   }

  ngOnInit(): void {
  }

  AddJob(cr){
    alert("sSuccessfull");
    let user = {
      //candidateid:this.auth.getCurrentUser()._id,
      //customerid:this.auth.getCurrentUser()._id,
      jobName:cr.value.registration.jobName,
      experience:cr.value.registration.experience,
      requiredage:cr.value.registration.requiredage,
      salary:cr.value.registration.salary,
      jobtype:cr.value.registration.jobtype
      
      
  
  
  }
  this.service.create(user).subscribe(Response => {
      this.toastr.success(`Job successfully added`)
    },
    (error: AppError) => {
      if (error instanceof BadInput)
        this.toastr.error('Incorrect Inputs');
      else
        throw error
   }
  )
  
  cr.reset();
  
  }
  
  
  }
