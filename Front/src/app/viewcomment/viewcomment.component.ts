import { AuthService } from './../services/auth.service';
import { CompanyService } from './../services/company.service';
import { ApplyjobsService } from './../services/applyjobs.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
//import { ApplyjobsService}

@Component({
  selector: 'viewcomment',
  templateUrl: './viewcomment.component.html',
  styleUrls: ['./viewcomment.component.css']
})
export class ViewcommentComponent implements OnInit {

  data=[]
  
  id
  constructor(private service:ApplyjobsService,private route:ActivatedRoute, private toastr: ToastrService,
    private companyservice:CompanyService,
    private auth:AuthService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      if(params.has("id")) {
         
          this.id = params.get('id');    
          this.service.getById(this.id).subscribe(
            data=> {
              this.data = data
            })
  }
})

}

reject(comment){
  console.log(comment);
  this.service.update({status:'rejected'},"jobupdate",comment._id)
  .subscribe(response=>{
    console.log(response);
  })
}

accept(comment){
  this.service.update({status:'accepted'},"jobupdate",comment._id)
  .subscribe(response=>{

    this.companyservice.update({assignid:comment.applicantid},"updatejob",this.id)
    .subscribe(response=>{
      console.log(response)
    })
    })}

  

}

