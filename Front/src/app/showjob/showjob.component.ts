import { JobService } from './../services/job.service';
import { Component, OnInit } from '@angular/core';
//import { JobService } from './../services/order.service';
//import { Component, OnInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';
import { NotFoundError } from './../common/not-found-error';
//import { OrderService } from './../services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';
import { Http } from '@angular/http';


@Component({
  selector: 'showjob',
  templateUrl: './showjob.component.html',
  styleUrls: ['./showjob.component.css']
})
export class ShowjobComponent implements OnInit {

  
  job:any = []
  data=[]
  constructor(private toastr: ToastrService,
    private http:Http,
    private router: Router,
    private service:JobService) { 
      this.service.getcap("all").subscribe(
        data=> {
          this.data = data
          console.log(this.data);
        }
      )
    }


    
  
  ngOnInit(): void {
  }
 
  confirmjob(Job){

     this.router.navigate(['/jobdone',Job._id]);
     
    // console.log(Job);    
  }


  Reject(Job){

    this.router.navigate(['/rejectjob', Job._id]);
  }
  
    
  
}