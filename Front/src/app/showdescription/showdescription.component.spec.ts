import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowdescriptionComponent } from './showdescription.component';

describe('ShowdescriptionComponent', () => {
  let component: ShowdescriptionComponent;
  let fixture: ComponentFixture<ShowdescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowdescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowdescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
