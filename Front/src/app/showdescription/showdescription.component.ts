import { AuthService } from './../services/auth.service';
import { JobService } from './../services/job.service';
import { Component, OnInit } from '@angular/core';
//import { JobService } from './../services/ride.service';
//import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'showdescription',
  templateUrl: './showdescription.component.html',
  styleUrls: ['./showdescription.component.css']
})
export class ShowdescriptionComponent implements OnInit {

  data=[]

  constructor(private toastr: ToastrService,
    private http:Http,
    private service:JobService,
    private auth: AuthService) { 
      // this.service.getAll().subscribe(
      //   data=> {
      //     this.data = data
      //   }
      // )
      this.http.get("http://localhost:3000/api/jobs/"+this.auth.getCurrentUser()._id)
      .subscribe(response=>{
        this.data = response.json();
        console.log(this.auth.getCurrentUser());
      })
    }
  ngOnInit(): void {
  }


 
}

