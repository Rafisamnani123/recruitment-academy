import { Component, OnInit } from '@angular/core';
import { CompanyService } from './../services/company.service';
//import { Component, OnInit } from '@angular/core';

import { NotFoundError } from './../common/not-found-error';

import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';
import { Http } from '@angular/http';

@Component({
  selector: 'myjobs',
  templateUrl: './myjobs.component.html',
  styleUrls: ['./myjobs.component.css']
})
export class MyjobsComponent implements OnInit {

  job:any = []
  data=[]
  constructor(private toastr: ToastrService,
    private http:Http,
    private router: Router,
    private service:CompanyService) { 
      this.service.getcap("view").subscribe(
        data=> {
          this.data = data
        }
      )
    }


    
  
  ngOnInit(): void {
  }
 
  applyjob(Job){

     this.router.navigate(['/viewcomment',Job._id]);
     
    // console.log(Job);    
  }
}
