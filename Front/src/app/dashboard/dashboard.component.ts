import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { Http } from '@angular/http';
import { AppError } from '../common/app-error';
import { NoAccess } from '../common/noaccess';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userType = 'Candidate'
  data = []

  

  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: UserService,
    private http:Http

    ) {
       http.get('http://localhost:3000/api/users/')
       .subscribe(response=>{
         this.data = response.json();
       } )
       
   }

  ngOnInit(): void {
	 this.service.getAll().subscribe(data=> {
	          this.data = data;
    }, 
    (error: AppError) => {
        if (error instanceof NoAccess) 
          console.error("Access Denied!");
        else
          throw error;
      }
    )
  }

  viewDetail(user) {
    this.router.navigate(['/userprofile', user._id]);
  }
  editUser(user) {
      this.router.navigate(['/signup', user._id]);
  }
  
}
