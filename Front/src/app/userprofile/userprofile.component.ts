import { Component, OnInit } from '@angular/core';
import { AuthService } from './../services/auth.service';
//import { Component, OnInit } from '@angular/core';
import { UserService } from './../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  profile
 
  constructor(private http: Http,
    private router: Router,
    private auth: AuthService){

      console.log(auth.getCurrentUser()._id);
      http.get('http://localhost:3000/api/users/'+auth.getCurrentUser()._id).subscribe(response => {
        this.profile=response.json();
        console.log(this.profile);
       
      })
    }

  ngOnInit(): void {
  }

}

