import { AuthService } from './../services/auth.service';
import { CompanyService } from './../services/company.service';
import { Component, OnInit } from '@angular/core';

import { NotFoundError } from './../common/not-found-error';

import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';
import { Http } from '@angular/http';

@Component({
  selector: 'showaddjob',
  templateUrl: './showaddjob.component.html',
  styleUrls: ['./showaddjob.component.css']
})
export class ShowaddjobComponent implements OnInit {
  userid:any
  job:any = []
  data=[]
  constructor(private toastr: ToastrService,
    private http:Http,
    private router: Router,
    private service:CompanyService,
    private auth:AuthService) { 
      this.service.getcap("view").subscribe(
        data=> {
          this.data = data
          console.log(data)
          this.userid=this.auth.getCurrentUser()._id;
          console.log(this.userid);
        }
      )
    }


    
  
  ngOnInit(): void {
    
    
  }
 
  applyjob(Job){

     this.router.navigate(['/applyjob',Job._id]);
     
    // console.log(Job);    
  }
}
