import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowaddjobComponent } from './showaddjob.component';

describe('ShowaddjobComponent', () => {
  let component: ShowaddjobComponent;
  let fixture: ComponentFixture<ShowaddjobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowaddjobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowaddjobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
