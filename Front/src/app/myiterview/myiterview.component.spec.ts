import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyiterviewComponent } from './myiterview.component';

describe('MyiterviewComponent', () => {
  let component: MyiterviewComponent;
  let fixture: ComponentFixture<MyiterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyiterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyiterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
