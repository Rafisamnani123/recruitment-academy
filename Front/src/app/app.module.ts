import { CompanyService } from './services/company.service';
import { JobService } from './services/job.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ɵallowSanitizationBypassAndThrow } from '@angular/core';
import { AdminAuthGuard } from './services/admin-auth-guard.service';
import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';


import { RouterModule } from '@angular/router';

//import { NgModule } from '@angular/core';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap'; 
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NoaccessComponent } from './noaccess/noaccess.component';
import { PostjobComponent } from './postjob/postjob.component';
import { ShowjobComponent } from './showjob/showjob.component';
import { JobdoneComponent } from './jobdone/jobdone.component';
import { RejectjobComponent } from './rejectjob/rejectjob.component';
import { ShowinterviewComponent } from './showinterview/showinterview.component';
import { ShowdescriptionComponent } from './showdescription/showdescription.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { AddjobComponent } from './addjob/addjob.component';
import { ShowaddjobComponent } from './showaddjob/showaddjob.component';
import { ApplyjobComponent } from './applyjob/applyjob.component';
import { MyjobsComponent } from './myjobs/myjobs.component';
import { ViewcommentComponent } from './viewcomment/viewcomment.component';
import { MyiterviewComponent } from './myiterview/myiterview.component';
import { Home2Component } from './home2/home2.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    NoaccessComponent,
    PostjobComponent,
    ShowjobComponent,
    JobdoneComponent,
    RejectjobComponent,
    ShowinterviewComponent,
    ShowdescriptionComponent,
    ProfileComponent,
    DashboardComponent,
    UserprofileComponent,
    AddjobComponent,
    ShowaddjobComponent,
    ApplyjobComponent,
    MyjobsComponent,
    ViewcommentComponent,
    MyiterviewComponent,
    Home2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,

    ToastrModule.forRoot(),
     
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
      {path:'signup/:id',component:SignupComponent},
      { path: '', component: Home2Component },
      { path: 'navbar', component: NavbarComponent },
      { path: 'noaccess', component: NavbarComponent },
      { path: 'postjob', component: PostjobComponent },
      { path: 'showjob', component: ShowjobComponent },
      { path: 'jobdone/:id', component: JobdoneComponent },
      { path: 'rejectjob/:id', component: RejectjobComponent },
      { path: 'showinterview', component: ShowinterviewComponent },
      { path: 'showdescription', component: ShowdescriptionComponent },
      { path: 'profile', component: ProfileComponent},
      {path:'userdashboard', component:DashboardComponent},
      {path:'userprofile/:id', component:UserprofileComponent},
      {path:'addjob', component:AddjobComponent},
      {path:'showaddjob', component:ShowaddjobComponent},
      {path:'applyjob/:id', component:ApplyjobComponent},
      {path:'myjobs', component:MyjobsComponent},
      {path:'viewcomment/:id', component:ViewcommentComponent},

      

      
    
    
    ]),
  ],
  providers: [

    AuthService,
    AuthGuard,
    AdminAuthGuard,
    JobService,
    CompanyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
