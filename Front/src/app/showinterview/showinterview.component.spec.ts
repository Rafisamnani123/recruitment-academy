import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowinterviewComponent } from './showinterview.component';

describe('ShowinterviewComponent', () => {
  let component: ShowinterviewComponent;
  let fixture: ComponentFixture<ShowinterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowinterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowinterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
