//import { Component, OnInit } from '@angular/core';
import { JobService } from './../services/job.service';
import { Component, OnInit } from '@angular/core';
//import { JobService } from './../services/order.service';
//import { Component, OnInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';
import { NotFoundError } from './../common/not-found-error';
//import { OrderService } from './../services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppError } from './../common/app-error';
import { Identifiers } from '@angular/compiler';
import { Http } from '@angular/http';

@Component({
  selector: 'showinterview',
  templateUrl: './showinterview.component.html',
  styleUrls: ['./showinterview.component.css']
})
export class ShowinterviewComponent implements OnInit {

  job:any = []
  data=[]
  status;
  constructor(private toastr: ToastrService,
    private http:Http,
    private router: Router,
    private service:JobService) { 
      this.service.getcap("all").subscribe(
        data=> {
          this.data = data
        }
      )
    }


    
  
  ngOnInit(): void {
  }
 
  isStatus(){
    if(this.status == "interview"){
      return true;
    }
    else{
      return false;
    }

    
  }

  accept(Job){
    this.service.update({status:'accepted'},"status",Job._id)
    .subscribe(response=>{
      console.log(response)
    })
  }

  reject(Job){
    this.service.update({status:'rejected'},"status",Job._id)
    .subscribe(response=>{
      console.log(response)
    })
  }
  
    
  
}

